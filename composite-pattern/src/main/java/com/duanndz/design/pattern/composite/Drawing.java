package com.duanndz.design.pattern.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created By duan.nguyen at 8/19/20 1:37 AM
 */
public class Drawing implements Shape {

    //collection of Shapes
    private final List<Shape> shapes = new ArrayList<>();

    @Override
    public void draw(String fillColor) {
        for (Shape sh : shapes) {
            sh.draw(fillColor);
        }
    }

    //adding shape to drawing
    public void add(Shape s) {
        this.shapes.add(s);
    }

    //removing shape from drawing
    public void remove(Shape s) {
        shapes.remove(s);
    }

    //removing all the shapes
    public void clear() {
        System.out.println("------------------------------------");
        System.out.println("Clearing all the shapes from drawing");
        this.shapes.clear();
    }
}
