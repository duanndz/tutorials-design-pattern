package com.duanndz.design.pattern.composite;

/**
 * Created By duan.nguyen at 8/19/20 1:26 AM
 */
public class Triangle implements Shape {

    @Override
    public void draw(String fillColor) {
        System.out.println("Drawing Triangle with color " + fillColor);
    }

}
