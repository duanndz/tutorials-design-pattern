package com.duanndz.design.pattern.composite;

/**
 * Created By duan.nguyen at 8/19/20 1:24 AM
 */
public interface Shape {

    void draw(String fillColor);

}
