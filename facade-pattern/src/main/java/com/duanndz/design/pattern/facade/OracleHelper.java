package com.duanndz.design.pattern.facade;

import java.sql.Connection;

/**
 * Created By duan.nguyen at 8/28/20 3:31 PM
 */
public class OracleHelper {

    public static Connection getOracleDBConnection() {
        //get Oracle DB connection using connection parameters
        return null;
    }

    public void generateOraclePDFReport(String tableName, Connection con) {
        //get data from table and generate pdf report
    }

    public void generateOracleHTMLReport(String tableName, Connection con) {
        //get data from table and generate pdf report
    }

}
