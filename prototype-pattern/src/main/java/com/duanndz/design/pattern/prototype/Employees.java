package com.duanndz.design.pattern.prototype;

import java.util.ArrayList;
import java.util.List;

/**
 * Created By duan.nguyen at 8/4/20 10:45 AM
 */
public class Employees implements Cloneable {

    private final List<String> empList;

    public Employees() {
        empList = new ArrayList<>();
    }

    public Employees(List<String> list) {
        this.empList = list;
    }

    public void loadData() {
        //read all employees from database and put into the list
        empList.add("Duan");
        empList.add("Khoi");
        empList.add("Nhan");
        empList.add("Hai");
    }

    public List<String> getEmpList() {
        return empList;
    }

    @Override
    public Employees clone() {
        List<String> temp = new ArrayList<>(this.getEmpList());
        return new Employees(temp);
    }
}
