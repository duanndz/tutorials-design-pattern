package com.duanndz.design.pattern.prototype;

/**
 * Created By duan.nguyen at 8/4/20 10:50 AM
 */
public class PrototypePatternExample {

    public static void main(String[] args) {
        var employees = new Employees();
        employees.loadData();

        var employeesNew = employees.clone();
        var employeesNew2 = employees.clone();
        employeesNew.getEmpList().add("Hang");
        employeesNew2.getEmpList().remove("Duan");
        System.out.println("employees: " + employees.getEmpList());
        System.out.println("employeesNew: " + employeesNew.getEmpList());
        System.out.println("employeesNew2: " + employeesNew2.getEmpList());
    }
}
