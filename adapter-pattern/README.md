# Adapter Pattern

- Adapter design pattern is one of the structural design pattern 
    and its used so that two unrelated interfaces can work together. 
    The object that joins these unrelated interface is called an Adapter.
  
1. Adapter Design Pattern
    - One of the great real life example of Adapter design pattern is mobile charger. 
        Mobile battery needs 3 volts to charge but the normal socket produces either 120V (US) or 240V (India). 
        So the mobile charger works as an adapter between mobile charging socket and the wall socket.
    - We will try to implement multi-adapter using adapter design pattern in this tutorial.
    - So first of all we will have two classes – Volt (to measure volts) 
        and Socket (producing constant volts of 120V).
    - Now we want to build an adapter that can produce 3 volts, 12 volts and default 120 volts. 
        So first of all we will create an adapter interface with these methods.

2. Two Way Adapter Pattern
    - While implementing Adapter pattern, there are two approaches 
        – class adapter and object adapter 
        – however both these approaches produce same result.
    - Class Adapter 
        – This form uses java inheritance and extends the source interface, in our case Socket class.
    - Object Adapter 
        – This form uses Java Composition and adapter contains the source object.

3. Adapter Design Pattern – Class Adapter
    - Here is the class adapter approach implementation of our adapter.
        
4. Adapter Design Pattern – Object Adapter Implementation
    - Notice that both the adapter implementations are almost same and 
        they implement the SocketAdapter interface. 
        The adapter interface can also be an abstract class.

5. 



      











      
  

      


