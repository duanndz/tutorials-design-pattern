package com.duanndz.design.pattern.adapter;

/**
 * Created By duan.nguyen at 8/11/20 11:53 AM
 */
public class SocketObjectAdapterImpl implements SocketAdapter {

    private final Socket socket = new Socket();

    @Override
    public Volt get120Volt() {
        return socket.getVolt();
    }

    @Override
    public Volt get12Volt() {
        return convertVolt(socket.getVolt(), 10);
    }

    @Override
    public Volt get3Volt() {
        return convertVolt(socket.getVolt(), 40);
    }

}
