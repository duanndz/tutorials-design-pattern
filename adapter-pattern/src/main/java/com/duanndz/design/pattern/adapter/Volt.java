package com.duanndz.design.pattern.adapter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Created By duan.nguyen at 8/11/20 11:35 AM
 */
@ToString(of = {"volts"})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Volt {

    private int volts;

}
