package com.duanndz.design.pattern.adapter;

/**
 * Created By duan.nguyen at 8/11/20 11:56 AM
 */
public class AdapterPatternExample {

    public static void main(String[] args) {
        SocketAdapter classAdapter = new SocketClassAdapterImpl();
        System.out.println(classAdapter.get120Volt());
        System.out.println(classAdapter.get12Volt());
        System.out.println(classAdapter.get3Volt());

        SocketAdapter objectAdapter = new SocketObjectAdapterImpl();
        System.out.println(objectAdapter.get120Volt());
        System.out.println(objectAdapter.get12Volt());
        System.out.println(objectAdapter.get3Volt());
    }
}
