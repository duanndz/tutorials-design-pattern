package com.duanndz.design.pattern.adapter;

/**
 * Created By duan.nguyen at 8/11/20 11:45 AM
 */
public interface SocketAdapter {

    Volt get120Volt();

    Volt get12Volt();

    Volt get3Volt();

    default Volt convertVolt(Volt v, int division) {
        return new Volt(v.getVolts() / division);
    }

}
