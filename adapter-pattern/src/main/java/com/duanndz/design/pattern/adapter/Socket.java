package com.duanndz.design.pattern.adapter;

/**
 * Created By duan.nguyen at 8/11/20 11:38 AM
 */
public class Socket {

    public Volt getVolt() {
        return new Volt(120);
    }

}
