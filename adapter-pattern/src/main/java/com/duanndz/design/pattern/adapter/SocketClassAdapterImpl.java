package com.duanndz.design.pattern.adapter;

/**
 * Created By duan.nguyen at 8/11/20 11:49 AM
 */
public class SocketClassAdapterImpl extends Socket implements SocketAdapter {

    @Override
    public Volt get120Volt() {
        return getVolt();
    }

    @Override
    public Volt get12Volt() {
        Volt source = getVolt();
        return convertVolt(source, 12);
    }

    @Override
    public Volt get3Volt() {
        Volt source = getVolt();
        return convertVolt(source, 40);
    }

}
