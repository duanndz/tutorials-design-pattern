package com.duanndz.design.pattern.builder;

/**
 * Created By duan.nguyen at 8/4/20 10:03 AM
 */
public class BuilderPatternExample {

    public static void main(String[] args) {
        // Using builder to get the object in a single line of code and
        // without any inconsistent state or arguments management issues
        var myComputer = new Computer
            .ComputerBuilder("16 GB", "512 GB", "3.1 Ghz")
            .bluetoothEnabled(true)
            .graphicsCardEnabled(false)
            .build();
        System.out.println(myComputer);
    }

}
