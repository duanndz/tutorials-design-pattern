package com.duanndz.design.pattern.builder;

import lombok.Getter;

/**
 * Created By duan.nguyen at 8/4/20 9:47 AM
 */
@Getter
public class Computer {

    private final String RAM;
    private final String HDD;
    private final String CPU;

    // optional params
    private final boolean graphicsCardEnabled;
    private final boolean bluetoothEnabled;

    private Computer(ComputerBuilder builder) {
        this.RAM = builder.RAM;
        this.HDD = builder.HDD;
        this.CPU = builder.CPU;
        this.graphicsCardEnabled = builder.graphicsCardEnabled;
        this.bluetoothEnabled = builder.bluetoothEnabled;
    }

    @Override
    public String toString() {
        return "Computer{"
            + "RAM='" + RAM + '\''
            + ", HDD='" + HDD + '\''
            + ", CPU='" + CPU + '\''
            + ", graphicsCardEnabled=" + graphicsCardEnabled
            + ", bluetoothEnabled=" + bluetoothEnabled
            + '}';
    }

    //Builder Class
    public static class ComputerBuilder {
        private final String RAM;
        private final String HDD;
        private final String CPU;
        private boolean graphicsCardEnabled;
        private boolean bluetoothEnabled;

        public ComputerBuilder(String ram, String hdd, String cpu) {
            this.RAM = ram;
            this.HDD = hdd;
            this.CPU = cpu;
        }

        public ComputerBuilder graphicsCardEnabled(boolean graphicsCardEnabled) {
            this.graphicsCardEnabled = graphicsCardEnabled;
            return this;
        }

        public ComputerBuilder bluetoothEnabled(boolean bluetoothEnabled) {
            this.bluetoothEnabled = bluetoothEnabled;
            return this;
        }

        public Computer build() {
            return new Computer(this);
        }
    }

}
