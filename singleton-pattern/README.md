# Singleton Pattern  
    - One of the Gangs of Four Design patterns and comes in the Creational Design Pattern category.
    - Only one instance of class.
    - Must have global access point to create the instance.
    
1. Singleton Pattern
    - Singleton pattern restricts the instantiation of a class 
        and ensures that only one instance of the class exists in the java virtual machine.
    - Singleton pattern is used for logging, drivers objects, caching and thread pool.
    - Singleton design pattern is also used in other design patterns 
        like Abstract Factory, Builder, Prototype, Facade etc.
    - Singleton design pattern is used in core java classes also, 
        for example java.lang.Runtime, java.awt.Desktop.
    
2. Java Singleton Pattern Implementation 
    - we have different approaches but all of them have the following common concepts.
    - Private constructor to restrict instantiation of the class from other classes.
    - Private static variable of the same class that is the only instance of the class.
    - Public static method that returns the instance of the class, 
        this is the global access point for outer world to get the instance of the singleton class.
    
3. Eager initialization
    - the instance of Singleton Class is created at the time of class loading, 
        this is the easiest method to create a singleton class but it has a drawback 
        that instance is created even though client application might not be using it.
    - If your singleton class is not using a lot of resources, this is the approach to use.
    - But in most of the scenarios, Singleton classes are created for resources such as File System, Database connections, etc.
    -  Also, this method doesn’t provide any options for exception handling.

4. Static block initialization
    - Static block initialization implementation is similar to eager initialization, 
        except that instance of class is created in the static block 
        that provides option for exception handling.
    -  Both eager initialization and static block initialization creates the instance 
        even before it’s being used and that is not the best practice to use. 
        So in further sections, we will learn how to create a Singleton class 
        that supports lazy initialization.
     
5. Lazy Initialization
   - Lazy initialization method to implement Singleton pattern creates the instance in the global access method. 
        Here is the sample code for creating Singleton class with this approach.
   - The above implementation works fine in case of the single-threaded environment 
        but when it comes to multithreaded systems, it can cause issues if multiple threads are inside the 
            if condition at the same time. 
        It will destroy the singleton pattern and both threads will get the different instances 
            of the singleton class. 
        In next section, we will see different ways to create a thread-safe singleton class.

6. Thread Safe Singleton
    - The easier way to create a thread-safe singleton class is to make the global access method synchronized, 
        so that only one thread can execute this method at a time. 
        General implementation of this approach is like the below class.
    - Above implementation works fine and provides thread-safety but it reduces the performance 
        because of the cost associated with the synchronized method, although we need it only 
        for the first few threads who might create the separate instances (Read: Java Synchronization). 
        To avoid this extra overhead every time, double checked locking principle is used. 
        In this approach, the synchronized block is used inside the 
            if condition with an additional check to ensure 
            that only one instance of a singleton class is created.

7. Bill Pugh Singleton Implementation
    - Prior to Java 5, java memory model had a lot of issues and 
        the above approaches used to fail in certain scenarios where too many threads 
        try to get the instance of the Singleton class simultaneously. 
        So Bill Pugh came up with a different approach to create the Singleton class using 
            an inner static helper class. 
        The Bill Pugh Singleton implementation goes like this;
    - Notice the private inner static class that contains the instance of the singleton class. 
        When the singleton class is loaded, 
            SingletonHelper class is not loaded into memory and only when someone calls the getInstance method, 
            this class gets loaded and creates the Singleton class instance.
    - This is the most widely used approach for Singleton class as it doesn’t require synchronization. 
        I am using this approach in many of my projects and it’s easy to understand and implement also.

8. Using Reflection to destroy Singleton Pattern     
    - Reflection can be used to destroy all the above singleton implementation approaches. 
        Let’s see this with an example class.
    - When you run the above test class, you will notice that hashCode of both the instances 
        is not same that destroys the singleton pattern. 
        Reflection is very powerful and used in a lot of frameworks like Spring and Hibernate, 
        do check out Java Reflection Tutorial.
     
9. Enum Singleton
    - To overcome this situation with Reflection, 
        Joshua Bloch suggests the use of Enum to implement Singleton design pattern 
            as Java ensures that any enum value is instantiated only once in a Java program. 
        Since Java Enum values are globally accessible, so is the singleton. 
        The drawback is that the enum type is somewhat inflexible; 
            for example, it does not allow lazy initialization.

10. Serialization and Singleton
    - Sometimes in distributed systems, we need to implement Serializable interface in Singleton class 
        so that we can store its state in the file system and retrieve it at a later point of time. 
        Here is a small singleton class that implements Serializable interface also.
    - The problem with serialized singleton class is that whenever we deserialize it, 
        it will create a new instance of the class. Let’s see it with a simple program.
    - So it destroys the singleton pattern, 
        to overcome this scenario all we need to do it provide the implementation of readResolve() method.
        ``` 
        protected Object readResolve() {
            return getInstance();
        }
        ```

11. Use synchronized block inside the if loop and volatile variable
    - Pros
        ```
            - Thread safety is guaranteed
            - Client application can pass arguments
            - Lazy initialization achieved
            - Synchronization overhead is minimal and applicable only for first few threads when the variable is null.
        ```
    - Cons:
        ```
            - Extra if condition
        ```
    - The local variable result seems unnecessary. 
        But, it’s there to improve the performance of our code. 
        In cases where the instance is already initialized (most of the time), 
            the volatile field is only accessed once (due to “return result;” instead of “return instance;”). 
            This can improve the method’s overall performance by as much as 25 percent.
      



      



















