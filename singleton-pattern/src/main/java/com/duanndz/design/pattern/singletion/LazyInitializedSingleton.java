package com.duanndz.design.pattern.singletion;

/**
 * Created By duan.nguyen at 7/30/20 11:43 AM
 */
public final class LazyInitializedSingleton implements LogInterface {

    private static LazyInitializedSingleton instance;

    private LazyInitializedSingleton() {

    }

    public static LazyInitializedSingleton getInstance() {
        if (instance == null) {
            instance = new LazyInitializedSingleton();
        }

        return instance;
    }

}
