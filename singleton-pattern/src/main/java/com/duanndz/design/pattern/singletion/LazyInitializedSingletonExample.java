package com.duanndz.design.pattern.singletion;

/**
 * Created By duan.nguyen at 7/30/20 11:46 AM
 */
public class LazyInitializedSingletonExample {

    public static void main(String[] args) {
        LazyInitializedSingleton.getInstance().log();
    }

}
