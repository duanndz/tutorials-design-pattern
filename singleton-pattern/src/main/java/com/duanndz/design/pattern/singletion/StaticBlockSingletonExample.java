package com.duanndz.design.pattern.singletion;

/**
 * Created By duan.nguyen at 7/30/20 11:35 AM
 */
public class StaticBlockSingletonExample {

    public static void main(String[] args) {
        StaticBlockSingleton.getInstance().log();
    }

}
