package com.duanndz.design.pattern.singletion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/**
 * Created By duan.nguyen at 7/31/20 2:26 PM
 */
public class SerializedSingletonExample {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        var instance1 = SerializedSingleton.getInstance();
        ObjectOutput out = new ObjectOutputStream(new FileOutputStream(new File("logs/filename.ser")));
        out.writeObject(instance1);
        out.close();

        //deserailize from file to object
        ObjectInput in = new ObjectInputStream(new FileInputStream(new File("logs/filename.ser")));
        var instance2 = (SerializedSingleton) in.readObject();
        in.close();
        System.out.println(instance1);
        System.out.println(instance2);
        System.out.println("instance1 equals instance2?: " + (instance1.equals(instance2)));
    }
}
