package com.duanndz.design.pattern.singletion;

/**
 * Created By duan.nguyen at 7/31/20 1:39 PM
 */
public class BillPughSingletonExample {

    public static void main(String[] args) {
        BillPughSingleton.getInstance().log();
    }

}
