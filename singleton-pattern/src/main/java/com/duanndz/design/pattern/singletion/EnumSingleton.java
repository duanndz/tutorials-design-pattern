package com.duanndz.design.pattern.singletion;

/**
 * Created By duan.nguyen at 7/31/20 2:10 PM
 */
public enum EnumSingleton implements LogInterface {

    INSTANCE;

}
