package com.duanndz.design.pattern.singletion;

/**
 * Created By duan.nguyen at 7/31/20 10:49 AM
 */
public class ThreadSafeSingleton implements LogInterface {

    private static volatile ThreadSafeSingleton instance;
    private static final Object mutex = new Object();

    private ThreadSafeSingleton() {

    }

    // Basic
    public static synchronized ThreadSafeSingleton getInstance() {
        if (instance == null) {
            System.out.println("Initial instance with getInstance");
            instance = new ThreadSafeSingleton();
        }
        return instance;
    }

    // Advance
    public static ThreadSafeSingleton getInstanceUsingDoubleLocking() {
        if (instance == null) {
            synchronized (ThreadSafeSingleton.class) {
                System.out.println("Initial instance using double locking.");
                if (instance == null) {
                    instance = new ThreadSafeSingleton();
                }
            }
        }
        return instance;
    }

    public static ThreadSafeSingleton getVolatileInstance() {
        var result = instance;
        if (result == null) {
            synchronized (mutex) {
                result = instance;
                if (result == null) {
                    System.out.println("Initial instance using synchronized block inside the if loop and volatile variable");
                    instance = result = new ThreadSafeSingleton();
                }
            }
        }
        return result;
    }

}
