package com.duanndz.design.pattern.singletion;

/**
 * Created By duan.nguyen at 7/31/20 2:10 PM
 */
public class EnumSingletonExample {

    public static void main(String[] args) {
        var instance1 = EnumSingleton.INSTANCE;
        instance1.log();
        // Java ensures that any enum value is instantiated only once in a Java program
        // Throw java.lang.IllegalArgumentException: Cannot reflectively create enum objects
        try {
            var constructors = EnumSingleton.class.getDeclaredConstructors();
            EnumSingleton instance2 = null;
            for (var con : constructors) {
                con.setAccessible(true);
                instance2 = (EnumSingleton) con.newInstance();
                break;
            }
            System.out.println(instance2);
            System.out.println("Enum singleton instance1 equals instance2?: " + (instance1.equals(instance2)));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
