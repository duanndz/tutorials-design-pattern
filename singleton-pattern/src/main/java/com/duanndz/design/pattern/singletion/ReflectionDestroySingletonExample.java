package com.duanndz.design.pattern.singletion;

/**
 * Created By duan.nguyen at 7/31/20 1:44 PM
 */
public class ReflectionDestroySingletonExample {

    public static void main(String[] args) {

        var instance1 = EagerInitializedSingleton.getInstance();
        System.out.println(instance1.toString());
        try {
            var constructors = EagerInitializedSingleton.class.getDeclaredConstructors();
            EagerInitializedSingleton instance2 = null;
            for(var con : constructors) {
                con.setAccessible(true);
                instance2 = (EagerInitializedSingleton) con.newInstance();
                break;
            }
            System.out.println(instance2);
            System.out.println("instance1 equals instance2?: " + (instance1.equals(instance2)));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
