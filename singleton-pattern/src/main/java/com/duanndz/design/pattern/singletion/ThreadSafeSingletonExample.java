package com.duanndz.design.pattern.singletion;

/**
 * Created By duan.nguyen at 7/31/20 10:50 AM
 */
public class ThreadSafeSingletonExample {

    public static void main(String[] args) {
        System.out.println("-----------------------------------------");
        ThreadSafeSingleton.getVolatileInstance().log();
        ThreadSafeSingleton.getInstanceUsingDoubleLocking().log();
        ThreadSafeSingleton.getInstance().log();
    }
}
