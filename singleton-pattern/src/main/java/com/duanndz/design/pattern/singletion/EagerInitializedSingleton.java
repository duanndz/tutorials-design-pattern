package com.duanndz.design.pattern.singletion;

/**
 * Created By duan.nguyen at 7/30/20 11:02 AM
 */
public final class EagerInitializedSingleton {

    private static final EagerInitializedSingleton INSTANCE = new EagerInitializedSingleton();

    private EagerInitializedSingleton() {
    }

    public static EagerInitializedSingleton getInstance() {
        return INSTANCE;
    }

    public void log() {
        System.out.println("Hello world!");
    }

}
