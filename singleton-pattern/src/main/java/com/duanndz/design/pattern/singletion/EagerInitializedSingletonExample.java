package com.duanndz.design.pattern.singletion;

/**
 * Created By duan.nguyen at 7/30/20 11:04 AM
 */
public class EagerInitializedSingletonExample {

    public static void main(String[] args) {
        var eagerInitializedSingleton = EagerInitializedSingleton.getInstance();
        eagerInitializedSingleton.log();
    }

}
