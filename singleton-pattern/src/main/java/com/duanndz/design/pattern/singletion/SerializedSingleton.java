package com.duanndz.design.pattern.singletion;

import java.io.Serializable;

/**
 * Created By duan.nguyen at 7/31/20 2:19 PM
 */
public final class SerializedSingleton implements Serializable, LogInterface {

    private static final long serialVersionUID = 1L;

    private SerializedSingleton() {

    }

    public static SerializedSingleton getInstance() {
        return SingletonHelper.INSTANCE;
    }

    protected Object readResolve() {
        return getInstance();
    }

    private static class SingletonHelper {

        private static final SerializedSingleton INSTANCE = new SerializedSingleton();

    }

}
