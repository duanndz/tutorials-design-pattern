package com.duanndz.design.pattern.singletion;

/**
 * Created By duan.nguyen at 7/30/20 11:34 AM
 */
public interface LogInterface {

    default void log() {
        System.out.println("-----------------------------------------");
        String message = String.format("It is %s at %s", this.getClass().getSimpleName(), this.toString());
        System.out.println(message);
    }
}
