package com.duanndz.design.pattern.singletion;

/**
 * Created By duan.nguyen at 7/30/20 11:30 AM
 */
public final class StaticBlockSingleton implements LogInterface {

    private static final StaticBlockSingleton INSTANCE;

    // static block initialization for exception handling
    static {
        try {
            INSTANCE = new StaticBlockSingleton();
        } catch (Exception ex) {
            throw new RuntimeException("Exception occur in creating singleton instance");
        }
    }

    private StaticBlockSingleton() {

    }

    public static StaticBlockSingleton getInstance() {
        return INSTANCE;
    }

}
