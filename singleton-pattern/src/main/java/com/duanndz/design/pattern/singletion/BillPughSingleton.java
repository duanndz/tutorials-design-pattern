package com.duanndz.design.pattern.singletion;

/**
 * Created By duan.nguyen at 7/31/20 1:21 PM
 */
public class BillPughSingleton implements LogInterface {

    private BillPughSingleton() {

    }

    public static BillPughSingleton getInstance() {
        return SingletonHelper.INSTANCE;
    }

    private static class SingletonHelper {
        private static final BillPughSingleton INSTANCE = new BillPughSingleton();
    }

}
