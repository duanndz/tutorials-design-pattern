package com.duanndz.design.pattern.decorator;

/**
 * Created By duan.nguyen at 9/14/20 3:17 PM
 */
public class BasicCar implements Car {

    @Override
    public void assemble() {
        System.out.print("Basic Car.");
    }

}
