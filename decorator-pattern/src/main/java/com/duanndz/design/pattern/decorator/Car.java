package com.duanndz.design.pattern.decorator;

/**
 * Created By duan.nguyen at 9/14/20 3:16 PM
 */
public interface Car {

    void assemble();

}
