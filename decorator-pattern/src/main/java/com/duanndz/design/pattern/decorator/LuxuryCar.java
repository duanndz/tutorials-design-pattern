package com.duanndz.design.pattern.decorator;

/**
 * Created By duan.nguyen at 9/14/20 3:26 PM
 */
public class LuxuryCar extends CarDecorator {

    public LuxuryCar(Car car) {
        super(car);
    }

    @Override
    public void assemble() {
        super.assemble();
        System.out.print(" Adding features of Luxury Car.");
    }
}
