package com.duanndz.design.pattern.decorator;

/**
 * Created By duan.nguyen at 9/14/20 3:25 PM
 */
public class SportsCar extends CarDecorator {

    public SportsCar(Car car) {
        super(car);
    }

    @Override
    public void assemble() {
        super.assemble();
        System.out.print(" Adding features of Sports Car.");
    }

}
