package com.duanndz.design.pattern.decorator;

/**
 * Created By duan.nguyen at 9/14/20 3:28 PM
 */
public class DecoratorPatternExample {

    public static void main(String[] args) {
        var sportsCar = new SportsCar(new BasicCar());
        sportsCar.assemble();
        System.out.println("\n*****");
        var sportsAndLuxuryCar = new SportsCar(new LuxuryCar(new BasicCar()));
        sportsAndLuxuryCar.assemble();
    }

}
