package com.duanndz.design.pattern.decorator;

/**
 * Created By duan.nguyen at 9/14/20 3:21 PM
 */
public class CarDecorator implements Car {

    protected final Car car;

    public CarDecorator(final Car car) {
        this.car = car;
    }

    @Override
    public void assemble() {
        this.car.assemble();
    }
}
