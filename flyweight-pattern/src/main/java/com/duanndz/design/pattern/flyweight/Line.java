package com.duanndz.design.pattern.flyweight;

import java.awt.*;

/**
 * Created By duan.nguyen at 8/24/20 9:41 AM
 */
public class Line implements Shape {

    public Line() {
        System.out.println("Creating Line object");
        //adding time delay to simulate a heavy object.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void draw(Graphics line, int x, int y, int width, int height, Color color) {
        line.setColor(color);
        line.drawLine(x, y, width, height);
    }

}
