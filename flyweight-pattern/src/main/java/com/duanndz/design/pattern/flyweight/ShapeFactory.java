package com.duanndz.design.pattern.flyweight;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created By duan.nguyen at 8/24/20 9:59 AM
 */
public class ShapeFactory {

    private static final Map<ShapeType, Shape> MAP = new ConcurrentHashMap<>();

    public static Shape getShape(ShapeType type) {
        var shapeImpl = MAP.get(type);
        if (shapeImpl == null) {
            if (type == ShapeType.OVAL_FILL) {
                shapeImpl = new Oval(true);
            } else if (type == ShapeType.OVAL_NOFILL) {
                shapeImpl = new Oval(false);
            } else if (type == ShapeType.LINE) {
                shapeImpl = new Line();
            }

            if (shapeImpl != null) {
                MAP.put(type, shapeImpl);
            }
        }
        return shapeImpl;
    }

    public static enum ShapeType {
        OVAL_FILL, OVAL_NOFILL, LINE
    }

}
