package com.duanndz.design.pattern.flyweight;

import java.awt.*;

/**
 * Created By duan.nguyen at 8/24/20 9:40 AM
 */
public interface Shape {

    void draw(Graphics g, int x, int y, int width, int height, Color color);

}
