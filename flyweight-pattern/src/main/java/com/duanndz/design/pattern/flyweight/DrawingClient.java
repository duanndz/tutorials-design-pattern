package com.duanndz.design.pattern.flyweight;

import javax.swing.*;
import java.awt.*;

import static com.duanndz.design.pattern.flyweight.ShapeFactory.ShapeType.LINE;
import static com.duanndz.design.pattern.flyweight.ShapeFactory.ShapeType.OVAL_FILL;
import static com.duanndz.design.pattern.flyweight.ShapeFactory.ShapeType.OVAL_NOFILL;

/**
 * Created By duan.nguyen at 8/24/20 10:15 AM
 */
public class DrawingClient extends JFrame {

    private static final long serialVersionUID = 1L;

    private static final ShapeFactory.ShapeType[] shapes = {LINE, OVAL_FILL, OVAL_NOFILL};
    private static final Color[] colors = {Color.RED, Color.GREEN, Color.YELLOW};

    private final int WIDTH;
    private final int HEIGHT;

    public DrawingClient(int width, int height) {
        this.WIDTH = width;
        this.HEIGHT = height;
        Container contentPane = getContentPane();

        JButton startButton = new JButton("Draw");
        final JPanel panel = new JPanel();

        contentPane.add(panel, BorderLayout.CENTER);
        contentPane.add(startButton, BorderLayout.SOUTH);
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        startButton.addActionListener(event -> {
            Graphics g = panel.getGraphics();
            for (int i = 0; i < 20; ++i) {
                Shape shape = ShapeFactory.getShape(getRandomShape());
                shape.draw(g, getRandomX(), getRandomY(), getRandomWidth(),
                    getRandomHeight(), getRandomColor());
            }
        });
    }

    private ShapeFactory.ShapeType getRandomShape() {
        return shapes[(int) (Math.random() * shapes.length)];
    }

    private int getRandomX() {
        return (int) (Math.random() * WIDTH);
    }

    private int getRandomY() {
        return (int) (Math.random() * HEIGHT);
    }

    private int getRandomWidth() {
        return (int) (Math.random() * (WIDTH / 10));
    }

    private int getRandomHeight() {
        return (int) (Math.random() * (HEIGHT / 10));
    }

    private Color getRandomColor() {
        return colors[(int) (Math.random() * colors.length)];
    }

    public static void main(String[] args) {
        new DrawingClient(500, 600);
    }

}
