package com.duanndz.design.pattern.factory;

import com.duanndz.design.pattern.factory.model.Computer;
import com.duanndz.design.pattern.factory.model.PC;
import com.duanndz.design.pattern.factory.model.Server;

/**
 * Created By duan.nguyen at 8/3/20 12:26 PM
 */
public class ComputerFactory {

    public static Computer getComputer(String type, String ram, String hdd, String cpu) {
        if ("PC".equalsIgnoreCase(type)) {
            return new PC(ram, hdd, cpu);
        } else if ("Server".equalsIgnoreCase(type)) {
            return new Server(ram, hdd, cpu);
        }
        return null;
    }
}
