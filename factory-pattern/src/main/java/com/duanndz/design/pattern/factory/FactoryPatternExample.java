package com.duanndz.design.pattern.factory;

/**
 * Created By duan.nguyen at 8/3/20 11:54 AM
 */
public class FactoryPatternExample {

    public static void main(String[] args) {
        var pc = ComputerFactory.getComputer("PC", "2 GB","500 GB","2.4 GHz");
        var server = ComputerFactory.getComputer("server", "16 GB","1 TB","2.9 GHz");
        System.out.println(pc);
        System.out.println(server);
    }
}
