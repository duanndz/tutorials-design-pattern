package com.duanndz.design.pattern.factory.model;

/**
 * Created By duan.nguyen at 8/3/20 11:54 AM
 */
public abstract class Computer {

    public abstract String getRAM();

    public abstract String getHDD();

    public abstract String getCPU();

    @Override
    public String toString() {
        return String.format("%s: RAM=%s, HDD=%s, CPU=%s", this.getClass().getSimpleName(), getRAM(), getHDD(), getCPU());
    }
}
