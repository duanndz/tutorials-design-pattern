package com.duanndz.design.pattern.bridge;

/**
 * Created By duan.nguyen at 9/14/20 2:19 PM
 */
public interface Color {

    void applyColor();

}
