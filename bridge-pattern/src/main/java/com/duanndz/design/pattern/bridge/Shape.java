package com.duanndz.design.pattern.bridge;

/**
 * Created By duan.nguyen at 9/14/20 2:20 PM
 */
public abstract class Shape {

    protected final Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public abstract void applyColor();

}
