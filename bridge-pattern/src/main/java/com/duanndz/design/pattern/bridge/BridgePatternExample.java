package com.duanndz.design.pattern.bridge;

/**
 * Created By duan.nguyen at 9/14/20 2:27 PM
 */
public class BridgePatternExample {

    public static void main(String[] args) {
        Shape tri = new Triangle(new RedColor());
        tri.applyColor();
        Shape pentagon = new Pentagon(new GreenColor());
        pentagon.applyColor();
    }

}
