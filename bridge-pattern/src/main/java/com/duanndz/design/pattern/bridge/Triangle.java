package com.duanndz.design.pattern.bridge;

/**
 * Created By duan.nguyen at 9/14/20 2:22 PM
 */
public class Triangle extends Shape {

    public Triangle(Color color) {
        super(color);
    }

    @Override
    public void applyColor() {
        System.out.print("Triangle filled with color ");
        color.applyColor();
    }
}
