package com.duanndz.design.pattern.bridge;

/**
 * Created By duan.nguyen at 9/14/20 2:26 PM
 */
public class GreenColor implements Color {

    @Override
    public void applyColor() {
        System.out.println("green.");
    }
}
