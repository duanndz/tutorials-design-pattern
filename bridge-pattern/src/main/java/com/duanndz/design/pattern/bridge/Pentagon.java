package com.duanndz.design.pattern.bridge;

/**
 * Created By duan.nguyen at 9/14/20 2:24 PM
 */
public class Pentagon extends Shape {

    public Pentagon(Color color) {
        super(color);
    }

    @Override
    public void applyColor() {
        System.out.print("Pentagon filled with color ");
        this.color.applyColor();
    }

}
