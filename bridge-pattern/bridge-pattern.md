# Bridge Design Pattern in Java 
---

    When we have interface hierarchies in both interfaces as well as implementations, 
    then bridge design pattern is used to decouple the interfaces from implementation 
    and hiding the implementation details from the client programs. 
    
1. Bridge Design Pattern
    - Just like Adapter pattern, bridge design pattern is one of the Structural design pattern.
    - According to GoF bridge design pattern is:
        - Decouple an abstraction from its implementation so that the two can vary independently
    - The implementation of bridge design pattern follows the notion 
        to prefer Composition over inheritance.    
       
2. Bridge Design Pattern in Java Example
    - If we look into bridge design pattern with example, it will be easy to understand. 
        Lets say we have an interface hierarchy in both interfaces and implementations like below image.
    - Now we will use bridge design pattern to decouple the interfaces from implementation. 
        UML diagram for the classes and interfaces after applying bridge pattern will look like below image.
    - Notice the bridge between Shape and Color interfaces and use of composition in implementing the bridge pattern.
    
