package com.duanndz.design.pattern.abstractfactory;

/**
 * Created By duan.nguyen at 8/3/20 1:09 PM
 */
public class AbstractFactoryPatternExample {

    public static void main(String[] args) {
        var pc = ComputerFactory.getComputer(new PCFactory("2 GB", "500 GB", "2.4 GHz"));
        var server = ComputerFactory.getComputer(new ServerFactory("16 GB", "1 TB", "2.9 GHz"));
        System.out.println(pc);
        System.out.println(server);
    }
}
