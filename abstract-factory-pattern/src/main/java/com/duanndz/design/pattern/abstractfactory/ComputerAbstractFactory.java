package com.duanndz.design.pattern.abstractfactory;

import com.duanndz.design.pattern.abstractfactory.model.Computer;

/**
 * Created By duan.nguyen at 8/3/20 1:36 PM
 */
public interface ComputerAbstractFactory {

    Computer createComputer();

}
