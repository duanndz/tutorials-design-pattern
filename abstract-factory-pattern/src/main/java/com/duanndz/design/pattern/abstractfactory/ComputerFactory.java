package com.duanndz.design.pattern.abstractfactory;

import com.duanndz.design.pattern.abstractfactory.model.Computer;

/**
 * Created By duan.nguyen at 8/3/20 1:43 PM
 */
public abstract class ComputerFactory {

    public static Computer getComputer(ComputerAbstractFactory factory) {
        return factory.createComputer();
    }

}
