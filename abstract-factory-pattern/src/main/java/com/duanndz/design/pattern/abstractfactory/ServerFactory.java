package com.duanndz.design.pattern.abstractfactory;

import com.duanndz.design.pattern.abstractfactory.model.Computer;
import com.duanndz.design.pattern.abstractfactory.model.Server;

/**
 * Created By duan.nguyen at 8/3/20 1:41 PM
 */
public class ServerFactory implements ComputerAbstractFactory {

    private final String ram;
    private final String hdd;
    private final String cpu;

    public ServerFactory(String ram, String hdd, String cpu) {
        this.ram = ram;
        this.hdd = hdd;
        this.cpu = cpu;
    }

    @Override
    public Computer createComputer() {
        return new Server(ram, hdd, cpu);
    }

}
