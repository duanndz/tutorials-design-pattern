package com.duanndz.design.pattern.abstractfactory;

import com.duanndz.design.pattern.abstractfactory.model.Computer;
import com.duanndz.design.pattern.abstractfactory.model.PC;

/**
 * Created By duan.nguyen at 8/3/20 1:40 PM
 */
public class PCFactory implements ComputerAbstractFactory {

    private final String ram;
    private final String hdd;
    private final String cpu;

    public PCFactory(String ram, String hdd, String cpu) {
        this.ram = ram;
        this.hdd = hdd;
        this.cpu = cpu;
    }

    @Override
    public Computer createComputer() {
        return new PC(ram, hdd, cpu);
    }
}
