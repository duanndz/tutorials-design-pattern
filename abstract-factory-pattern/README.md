# Abstract Factory Pattern
--- 
- Abstract Factory pattern is similar to Factory pattern and it’s a **factory of factories**. 
    If you are familiar with the factory design pattern in java, 
        you will notice that we have a single Factory class that returns the different sub-classes 
        based on the input provided and the factory class uses if-else or switch statements to achieve this.
- In Abstract Factory pattern, we get rid of if-else block and have a factory class for each sub-class 
    and then an Abstract Factory class that will return the sub-class based on the input factory class.
- Abstract Factory design pattern is one of the Creational patterns. 
    Abstract Factory pattern is almost similar to Factory Pattern 
        except the fact that its more like factory of factories.
- Ref: https://cdn.journaldev.com/wp-content/uploads/2013/06/Abstract-Factory-Pattern.png
    
1. Abstract Factory
    - If you are familiar with factory design pattern in java, 
        you will notice that we have a single Factory class. 
        This factory class returns different subclasses based on the input provided 
            and factory class uses if-else or switch statement to achieve this.
    - In the Abstract Factory pattern, we get rid of if-else block and have a factory class for each sub-class. 
        Then an Abstract Factory class that will return the sub-class based on the input factory class. 
        At first, it seems confusing but once you see the implementation, 
            it’s really easy to grasp and understand the minor difference between Factory and Abstract Factory pattern.

2. Factory Class for Each subclass
    - First of all we need to create an Abstract Factory interface or abstract class.
    - Notice that createComputer() method is returning an instance of super class Computer. 
        Now our factory classes will implement this interface and return their respective sub-class.
    - Notice that it's a simple class and getComputer method is accepting ComputerAbstractFactory argument 
        and returning Computer object. At this point the implementation must be getting clear.
    - 
 

   

     

   
