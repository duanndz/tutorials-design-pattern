package com.duanndz.design.pattern.proxy;

/**
 * Created By duan.nguyen at 8/19/20 3:29 PM
 */
public class ProxyPatternExample {

    public static void main(String[] args) {
        CommandExecutor executor = new CommandExecutorProxy("admin", "wrong_pwd");
        try {
            executor.runCommand("pwd");
            executor.runCommand(" rm -rf abc.pdf");
        } catch (Exception e) {
            System.out.println("Exception Message::" + e.getMessage());
        }
    }
}
