package com.duanndz.design.pattern.proxy;

/**
 * Created By duan.nguyen at 8/19/20 3:10 PM
 */
public class CommandExecutorImpl implements CommandExecutor {

    @Override
    public void runCommand(String cmd) throws Exception {
        //some heavy implementation
        Runtime.getRuntime().exec(cmd);
        System.out.println("'" + cmd + "' command executed.");
    }

}
