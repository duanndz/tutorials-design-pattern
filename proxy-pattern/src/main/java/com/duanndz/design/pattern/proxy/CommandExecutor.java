package com.duanndz.design.pattern.proxy;

/**
 * Created By duan.nguyen at 8/19/20 9:37 AM
 */
public interface CommandExecutor {

    void runCommand(String cmd) throws Exception;

}
