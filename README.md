# Design Pattern Tutorials
    Examples of Design Patterns in Java.
    
## Creational Pattern
- Creational design patterns provide solution to instantiate an object in the best possible way for specific situations.

1. Examples of Singleton Pattern in Java [singleton-pattern](singleton-pattern/README.md)

2. Examples of Factory Pattern in Java [factory-pattern](factory-pattern/README.md)

3. Examples of Abstract Factory Pattern in Java [abstract-factory-pattern](abstract-factory-pattern/README.md)

4. Examples of Builder Pattern in Java [builder-pattern](./builder-pattern/README.md)

5. Examples of Prototype Pattern in Java [prototype-pattern](./prototype-pattern/README.md)

##  Structural Design Patterns.
- Structural patterns provide different ways to create a class structure, 
    for example using inheritance and composition to create a large object from small objects.

6. Examples of Adapter Pattern in Java [adapter-pattern](./adapter-pattern/README.md)

7. Examples of Composite Pattern in Java [composite-pattern](./composite-pattern/README.md)

8. Examples of Proxy Pattern in Java [proxy-pattern](./proxy-pattern/proxy-pattern.md)

9. Examples of Flyweight Pattern in Java [flyweight-pattern](./flyweight-pattern/flyweight-pattern.md)

10. Examples of Facade Pattern in Java [facade-pattern](./facade-pattern/facade-pattern.md)

11. Examples of Bridge Pattern in Java [bridge-pattern](./bridge-pattern/bridge-pattern.md)

12. Examples of Decorator Pattern in java [decorator-pattern](./decorator-pattern/README.md)
